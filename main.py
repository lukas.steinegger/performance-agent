from slashprocstat import *
from slashprocmem import *
import influx

import argparse, asyncio, sys
from pprint import pprint

def initArgParser():
    parser = argparse.ArgumentParser(
                    prog = 'Performance-Agent',
                    description = 'desc TODO')

    parser.add_argument("pid", nargs='?', type=int) # TODO support selection of multiple PIDs  
    parser.add_argument("-s", "--seconds", type=int, default=4)

    #parser.add_argument("--inlfux", action="store_true")

    parser.add_argument("--mem", action="store_true")
    parser.add_argument("--cpu", action="store_true")
    parser.add_argument("--influx-domain", type=str, default="localhost")
    parser.add_argument("--influx-port", type=str, default=8086)
    parser.add_argument("--influx-db", type=str, default="TMP")
    parser.add_argument("--local", action="store_true")
    return parser


async def monitor_pid_load(pid, sleep, callback_function):
    counts_towards_system_usage = ["user", "nice", "system"]
    last_ps = proc_stat()
    last_pps = proc_pid_stat(pid)
    while True:
        #time.sleep(sleep)
        await asyncio.sleep(sleep)
        now_ps = proc_stat()
        now_pps = proc_pid_stat(pid)

        #print(last_ps)

        last_total_cpu = {cpu: sum(last_ps[cpu][k] for k in last_ps[cpu].keys() if k in counts_towards_system_usage) for cpu in last_ps.keys() if cpu.startswith("cpu")}
        last_total_cpu = sum(last_total_cpu.values())

        now_total_cpu = {cpu: sum(now_ps[cpu][k] for k in now_ps[cpu].keys() if k in counts_towards_system_usage) for cpu in now_ps.keys() if cpu.startswith("cpu")}
        #print({k:v for (k,v) in now_total_cpu.items() if k.startswith("cpu")})
        now_total_cpu = sum(now_total_cpu.values())

        total_cpu = now_total_cpu - last_total_cpu

        last_total_pid = last_pps["utime"] + last_pps["stime"]
        now_total_pid = now_pps["utime"] + now_pps["stime"]

        total_pid = now_total_pid - last_total_pid

        #print(f"total_pid {total_pid}")
        #print(f"total_cpu {total_cpu}")

        load = total_pid / total_cpu if total_cpu != 0 else 0

        callback_function(influx.from_pid_load(pid, load))

        last_ps = now_ps
        last_pps = now_pps


async def monitor_cpu_loop(sleep, callback_function):
    last = proc_stat()
    while True:
        await asyncio.sleep(sleep)
        #time.sleep(sleep)
        now = proc_stat()
        cpu_diff = {cpu: {key: now[cpu][key] - last[cpu][key] for key in last[cpu].keys()} for cpu in last.keys() if cpu.startswith("cpu")}
        cpu_diff_total = {cpu: sum(cpu_diff[cpu].values()) for cpu in cpu_diff.keys()}
        perc = {cpu:{key:(value / cpu_diff_total[cpu]) for key, value in cpu_diff[cpu].items()} for cpu in cpu_diff.keys()}
        #callback_function(influx.from_procstat(perc))
        #print(influx.from_procstat(perc))

        for entry in influx.from_procstat(perc):
            callback_function(entry)


async def monitor_mem(sleep, callback_function):
    while True:
        await asyncio.sleep(sleep)
        meminfo = proc_meminfo()
        #json.dump(meminfo, sys.stdout, indent=4)
        for entry in influx.from_meminfo(meminfo):
            callback_function(entry)
        #print(influx_protocol.from_meminfo(meminfo))


def main():
    args = initArgParser().parse_args(sys.argv[1:])
    
    #if args.raw and args.pid:
    #    pps = proc_pid_stat(args.pid)
    #    json.dump(pps, sys.stdout, indent=4)
    #ps = proc_stat()
    #del ps["intr"]
    #json.dump(ps, sys.stdout, indent=4)

    sender = lambda entry : print(entry)

    if not args.local:
        sender_ = influx.send2InflxDB(args.influx_domain, args.influx_port, args.influx_db, debug=True)
        if sender_ is None:
            return
        
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    used = False
    try:
        if args.mem:
            asyncio.ensure_future(monitor_mem(args.seconds, sender), loop=loop)
            used = True
        if args.pid:
            asyncio.ensure_future(monitor_pid_load(args.pid, args.seconds, sender), loop=loop)
            used = True
        if args.cpu:
            asyncio.ensure_future(monitor_cpu_loop(args.seconds, sender), loop=loop)
            used = True
        
        if used:
            loop.run_forever()
        else:
            print("Please pass an pid or the flags --cpu or --mem. to the program.")
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()


if __name__ == "__main__":
    main()