from utils import getFileContent


from pprint import pprint


def parse_proc_meminfo_line(meminfoline):
    row = meminfoline.replace(":", " ").split()

    # ret  parameter name, value, unit
    return row[0], int(row[1]), row[2] if len(row) > 2 else None
    

def parse_proc_meminfo(meminfo):
    #print(meminfo)
    entries = {}
    for line in meminfo:
        param, value, unit = parse_proc_meminfo_line(line)
        entries[param] = (value, unit)
    return entries
    

def proc_meminfo():
    meminfo = getFileContent("/proc/meminfo")
    meminfo = parse_proc_meminfo(meminfo)
    return meminfo


