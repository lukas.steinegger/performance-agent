#from influxdb_client import InfluxDBClient
#from influxdb_client.client.write_api import SYNCHRONOUS


#client = InfluxDBClient(host="localhost", port=8086)

import time, datetime, socket, requests, sys

def hostname():
    return socket.gethostname()


def timestamp():
    return int(time.mktime(datetime.datetime.now().timetuple()))


def procstatentry(cpu, values):
    tags = ",".join([f"host={hostname()}", f"cpu={cpu}"])
    fields = ",".join([f"{k}={v}" for k,v in values.items()])
    return f"cpu,{tags} {fields} {timestamp()}"


def from_procstat(procstat):
    for cpu, vals in procstat.items():
        yield procstatentry(cpu, vals)
    #return [procstatentry(cpu, vals) for cpu, vals in procstat.items()]


def from_meminfo(meminfo):
    for param, values in meminfo.items():
        tags = ",".join([f"host={hostname()}", f"name={param}"])
        fields = f"value={values[0]},unit={values[1]}"
        yield f"mem,{tags} {fields} {timestamp()}"

def from_pid_load(pid, load):
    return f"process,pid={pid} pis_cpu_load_total={load} {timestamp()}"


def send2InflxDB(domain, port, db, debug=False):
    def sender(data):
        if debug:
            print(data)
        requests.post(f"http://{domain}:{port}/write?db={db}", data=data)
    
    url = f"http://{domain}:{port}/health"

    try:
        response = requests.get(url)
        if response.status_code == 200:
            return sender
        else:
            raise ValueError(f"InfluxDB instance at {url} not heathy!")
    except requests.exceptions.RequestException:
        print(f"InlfuxDB instance at {url} not responding!", file=sys.stderr)

    
        