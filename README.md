# performance-agent

Tool needs Python version 3.10.

## Usage

```bash
// gets the cpu usage in a 4 second interval
python3 main.py --seconds 4

// gets the cpu usage of the process with the given pid (=1)
python3 main.py 1 
```

## Development help

```bash
podman pull docker.io/influxdb
podman run -i -p8086:8086 --rm docker.io/library/influxdb 
```