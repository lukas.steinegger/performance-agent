import os
from utils import getFileContent

def parse_proc_pid_stat(pidstat):
    names = ["pid", "comm", "state", "ppid", "pgrp",
        "session", "tty_nr", "tpgid", "flags", "minflt",
        "cminflt", "majflt", "cmajflt", "utime", "stime",
        "cutime", "cstime", "priority", "nice", "num_threads",
        "itrealvalue", "starttime", "vsize", "rss", "rsslim",
        "startcode", "endcode", "startstack", "kstkesp",
        "kstkeip", "signal", "blockes", "sigignore", "sigcatch",
        "wchan", "nswap", "cnswap", "exit_signal", "processor",
        "rt_priority", "policy", "delavacct_blkio_ticks",
        "guest_time", "cguest_time", "start_data", "end_data",
        "start_brk", "arg_start", "arg_end", "env_atart",
        "env_end", "exit_code"]
    #fields = [field for field in pidstat.split()]
    fields = pidstat.split()
    fields = {key: int(value) if key not in ["comm", "state"] else value for key, value in zip(names, fields)}
    return fields


def proc_pid_stat(pid: int):
    pidstat = getFileContent(f"/proc/{pid}/stat")[0] # should only have one entry / line TODO check?
    return parse_proc_pid_stat(pidstat)


def parse_line_proc_stat(name, vals):
    if name.startswith("cpu"):
        names = ["user", "nice", "system", "idle", "iowait", "irq",
            "softirq", "steal", "guest", "guest_nice"
        ]
        return {k: int(v) for k, v in zip(names, vals)}

    if name in ["intr", "softirq"]:
        return [int(v) for v in vals]

    if name in ["ctxt", "btime", "processes", "procs_running", "procs_blocked"]:
        if len(vals) > 1:
            return None
        return int(vals[0])
    return None


def parse_proc_stat(procstat):
    lines = {}
    for line in procstat:
        k, *v = line.split()
        lines[k] = parse_line_proc_stat(k, v)
    return lines


def proc_stat():
    procstat = getFileContent("/proc/stat")
    return parse_proc_stat(procstat)


def sc_clk_tck():
    # https://www.idnt.net/en-US/kb/941772
    return os.sysconf(os.sysconf_names['SC_CLK_TCK'])


def _parse_proc_uptime(procuptime):
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-uptime
    sec, idle = procuptime[0].split()
    return {"uptime":float(sec), "idle":float(idle)}


def proc_uptime():
    uptime = getFileContent("/proc/uptime")
    return _parse_proc_uptime(uptime)


def parse_proc_pid_status():
    pass


def proc_pid_status(pid):
    content = getFileContent(f"/proc/{pid}/status")
    

def cpuusage(ppid, uptime):
    # https://www.baeldung.com/linux/total-process-cpu-usage
    #TODO is that right ?? is ppid["starttime"] already in seconds?????
    elapsedtime = uptime["uptime"] * sc_clk_tck() - ppid["starttime"]
    totaltime = ppid["utime"] + ppid["stime"]
    return totaltime, elapsedtime

def cpuusage_diff(ppid0, ppid1):
    # # https://www.baeldung.com/linux/total-process-cpu-usage
    #elapsedtime = uptime["uptime"] * sc_clk_tck() - ppid["starttime"]
    tot0 = ppid0["utime"] + ppid0["stime"]
    tot1 = ppid1["utime"] + ppid1["stime"]
    print(tot0, tot1)
    return tot1 - tot0

